/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Tobi-Morph-PC
 */
public class PersistenceConstantUtil {

    public final static String BUSINESS_OWNER_ACCESS_GROUP = "Business Owner AG";
    public final static String BUSINESS_MANAGER_ACCESS_GROUP = "Business Manager AG";
    public final static String BUSINESS_OWNER_ACCESS_GROUP_DESC = "Business Owner Access Group";
    public final static String BUSINESS_MANAGER_ACCESS_GROUP_DESC = "Business Manager Access Group";
    public final static String BUSINESS_OWNER_USER_GROUP = "Business Owner UG";
    public final static String BUSINESS_MANAGER_USER_GROUP = "Business Manager UG";
    public final static String BUSINESS_OWNER_USER_GROUP_DESC = "Business Owner User Group";
    public final static String BUSINESS_OWNER_REQUEST = "Business Owner Request";
    public final static String BUSINESS_ATTENDANT = "Attendant";
    public final static String BUSINESS_LOGISTIC_MANAGER = "Logistics Manager";
    public final static String BUSINESS_DISPTACHER = "Dispatch Rider";

    public final static String TIME_BEFORE_PAGE_RELOAD = "Time Before Page Reload";

    public final static String SUPER_ADMIN_EMAIL = "superadmin@morphinnovations.com";

    //Paystack 
    public static String SECRET_KEY = "sk_live_72be113dc4c83ca68a94640403cf09683c1c484f";
    public static String PUBLIC_KEY = "pk_live_a48f213729baebdee1c57d38c83b8a54a0be03bd";
    public static String OWNER_EMAIL = "info@morphinnovations.com";
    public static String PAYSTACK_NAME = "Paystack Payment Plaform";
    public static String PAYSTACK_FLAT_CHARGE = "Paystack Flat Charge";
    public static String PAYSTACK_PERCENTAGE_CHARGE = "Paystack Percentage Charge";

    public static final String SALT = "chef-salt-morph";

    public static String generateHash(String input) {
        StringBuilder hash = new StringBuilder();

        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            byte[] hashedBytes = sha.digest(input.getBytes());
            char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f'};
            for (int idx = 0; idx < hashedBytes.length; ++idx) {
                byte b = hashedBytes[idx];
                hash.append(digits[(b & 0xf0) >> 4]);
                hash.append(digits[b & 0x0f]);
            }
        } catch (NoSuchAlgorithmException e) {
            // handle error here.
        }

        return hash.toString();
    }

    //List of Nigerian Banks
    public enum Banks {

        ACCESS_BANK("Access Bank", "044", "044150149"),
        CITIBANK_NIGERIA("CitiBank Nigeria", "023", "023150005"),
        DIAMOND_BANK("Diamond Bank", "063", "063150162"),
        ECOBANK_NIGERIA("Ecobank Nigeria", "050", "050150010"),
        ENTERPRISE_BANK("Enterprise Bank", "084", "084150015"),
        FIDELITY_BANK("Fidelity Bank", "070", "070150003"),
        FIRST_BANK("First Bank", "011", "011151003"),
        FIRST_CITY_MONUMENT_BANK("FCMB", "214", "214150018"),
        GUARANTY_TRUST_BANK("Guaranty Trust Bank", "058", "058152036"),
        HERTIAGE_BANK("Heritage Bank", "030", "030159992"),
        KEYSTONE_BANK("Keystone Bank", "082", "082150017"),
        MAINSTREET_BANK("Mainstreet Bank", "014", "014150331"),
        SKYE_BANK("Skye Bank", "076", "076151006"),
        STANBIC_IBTC_BANK("Stanbic IBTC Bank", "221", "221159522"),
        STANDARD_CHARTERED_BANK("Standard Chartered Bank", "068", "068150015"),
        STERLING_BANK("Sterling Bank", "232", "232150016"),
        UNION_BANK_OF_NIGERIA("Union Bank", "032", "032080474"),
        UNITED_BANK_FOR_AFRICA("UBA", "033", "033153513"),
        UNITY_BANK("Unity Bank", "215", "215154097"),
        WEMA_BANK("Wema Bank", "035", "035150103"),
        ZENITH_BANK("Zenith Bank", "057", "057150013"),
        JAIZ_BANK("Jaiz Bank", "301", "301080020"),
        SUNTRUST_BANK("Suntrust Bank", "", "");

        private final String name;
        private final String shortCode;
        private final String longCode;

        private Banks(String name, String shortCode, String longCode) {
            this.name = name;
            this.shortCode = shortCode;
            this.longCode = longCode;
        }

        public String getName() {
            return name;
        }

        public String getShortCode() {
            return shortCode;
        }

        public String getLongCode() {
            return longCode;
        }

    }

}

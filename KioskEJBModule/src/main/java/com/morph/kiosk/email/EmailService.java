/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.email;

import com.morph.kiosk.persistence.entity.portal.PortalRequest;
import com.morph.kiosk.persistence.entity.portal.PortalUserRequestLog;
import com.morph.kiosk.persistence.service.PortalPersistence;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Tobi-Morph-PC
 */
@Stateless
@Asynchronous
public class EmailService {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    private PortalPersistence persistence;

    @Inject
    private Event<Emailer> emailerEvent;

    public EmailService() {
    }

    public void sendMessage(@Observes Emailer email) {

        try {
            EmailSetting setting = EmailSetting.build();
            MimeMessage mimeMessage = new MimeMessage(setting.getMailSession());
            mimeMessage.setFrom(new InternetAddress(EmailSetting.getFROM()));
            mimeMessage.setSender(new InternetAddress(EmailSetting.getFROM()));
            mimeMessage.setSubject(email.getSubject());
            mimeMessage.setContent(email.getMessage(), "text/html");

            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(email.getRecipient()));

            Transport transport = setting.getMailSession().getTransport("smtp");
            transport.connect(EmailSetting.getHOST(), EmailSetting.getPORT(), EmailSetting.getUSER(), EmailSetting.getPASSWORD());

            transport.sendMessage(mimeMessage, mimeMessage.getRecipients(Message.RecipientType.TO));
            System.out.println("Message have been sent !!!!!!!!!");
            transport.close();
            email.setMessageSentStatus(true);
        } catch (MessagingException ex) {
            Logger.getLogger(EmailService.class.getName()).log(Level.SEVERE, null, ex);
        }

        persistRequestEmailDetail(email);
    }

    public void prepareEmail(String message, String subject, String recipient, String uuid, PortalRequest request) {
        emailerEvent.fire(new Emailer(message, subject, recipient, uuid, request));
    }

    private void persistRequestEmailDetail(Emailer email) {
        try {
            if (email != null && email.getRequest() != null) {
                PortalRequest pr = persistence.find(PortalRequest.class, email.getRequest().getId());
                PortalUserRequestLog log = new PortalUserRequestLog();
                log.setMessageStatus(email.isMessageSentStatus());
                log.setMessage(email.getMessage());
                log.setSubject(email.getSubject());
                log.setCodeStatus(true);
                log.setRequest(pr);
                log.setUser(pr.getUser() == null ? null : pr.getUser());
                log.setDateSent(new Date());
                log.setVerificationCode(String.valueOf(email.getUuid()));
                System.out.println("Printing PortalUserRequestLog ::::: " + log);
                persistence.create(log);
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}

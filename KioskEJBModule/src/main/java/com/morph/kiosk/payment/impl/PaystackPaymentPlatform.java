/**
*Class Name: PaystackPaymentPlatform
*Project Name: KioskEJBModule
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Feb 28, 2017 11:40:23 AM
*(C)Morph Innovations Limited 2017. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.payment.impl;

import com.morph.kiosk.payment.interfaces.PaymentPlatform;
import com.morph.kiosk.payment.transferobjects.PaymentResponse;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
public class PaystackPaymentPlatform implements PaymentPlatform{

    @Override
    public PaymentResponse applyPayment(double amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getApplicablCharges(double orderCost) {
        
        if(orderCost < 2500){
            return (orderCost * 0.15);
        }else{
            return (orderCost * 0.15) + 100;
        }
    }

}
 
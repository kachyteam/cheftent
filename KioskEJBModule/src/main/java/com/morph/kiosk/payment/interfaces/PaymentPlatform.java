/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.payment.interfaces;

import com.morph.kiosk.payment.transferobjects.PaymentResponse;

/**
 *
 * @author Onyedika
 */
public interface PaymentPlatform {
    
    public PaymentResponse applyPayment(double amount);
    
    public double getApplicablCharges(double orderCost);
    
}

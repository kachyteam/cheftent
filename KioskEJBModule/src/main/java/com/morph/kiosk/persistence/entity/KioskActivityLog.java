/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Tobi-Morph-PC This class keeps log of update activities made on
 * <code>Kiosk</code>. This class also keeps track of the modifier i.e
 * <code><pre>modifiedBy</pre> KioskUser</code>
 */
@Entity
public class KioskActivityLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String description;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDate;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedDate;
    private boolean status;
    private String creator;
    private String geolocation;
    private String address;
    private String longitude;
    private String latitude;
    private String imageUrl;
    @ManyToOne
    private Business business;
    @ManyToOne
    private KioskUser owner;
    private Double minOrderAmount;
    @ManyToOne
    private KioskUser modifieredBy;
    
    private Long kioskId;

    public KioskActivityLog(Kiosk kiosk) {
        name = kiosk.getName();
        description = kiosk.getDescription();
        createdDate = kiosk.getCreatedDate();
        modifiedDate = kiosk.getModifiedDate();
        status = kiosk.isStatus();
        creator = kiosk.getCreator();
        geolocation = kiosk.getGeolocation();
        address = kiosk.getAddress();
        longitude = kiosk.getLongitude();
        latitude = kiosk.getLatitude();
        imageUrl = kiosk.getImageUrl();
        business = kiosk.getBusiness();
        owner = kiosk.getOwner();
        minOrderAmount = kiosk.getMinOrderAmount();
        kioskId = kiosk.getId();
    }

    public KioskActivityLog() {
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KioskActivityLog)) {
            return false;
        }
        KioskActivityLog other = (KioskActivityLog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.KioskActivityLog[ id=" + id + " ]";
    }

    public KioskUser getModifieredBy() {
        return modifieredBy;
    }

    public void setModifieredBy(KioskUser modifieredBy) {
        this.modifieredBy = modifieredBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(String geolocation) {
        this.geolocation = geolocation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public KioskUser getOwner() {
        return owner;
    }

    public void setOwner(KioskUser owner) {
        this.owner = owner;
    }

    public Double getMinOrderAmount() {
        return minOrderAmount;
    }

    public void setMinOrderAmount(Double minOrderAmount) {
        this.minOrderAmount = minOrderAmount;
    }

    public Long getKioskId() {
        return kioskId;
    }

    public void setKioskId(Long kioskId) {
        this.kioskId = kioskId;
    }

}

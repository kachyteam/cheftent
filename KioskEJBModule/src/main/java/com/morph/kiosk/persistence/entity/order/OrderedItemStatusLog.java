/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.order;

import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.order.OrderedItem.Status;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Tobi-Morph-PC
 * This class keeps log of update activities made on <code>OrderedItem</code>.
 * This class also keeps track of the modifier i.e <code><pre>modifiedBy</pre> KioskUser</code>, also keeps log of the change made on customer's order at every stage.
 */
@Entity
public class OrderedItemStatusLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private OrderedItem item;

    @ManyToOne
    private Kiosk kiosk;

    @Enumerated(EnumType.STRING)
    private Status orderStatus;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDate;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @ManyToOne
    private KioskUser updatedby;
    
    @ManyToOne
    private KioskUser nextUpdateBy;

    private String comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderedItemStatusLog)) {
            return false;
        }
        OrderedItemStatusLog other = (OrderedItemStatusLog) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.OrderedItemStatusLog[ id=" + id + " ]";
    }

    public KioskUser getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(KioskUser updatedby) {
        this.updatedby = updatedby;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public OrderedItem getItem() {
        return item;
    }

    public void setItem(OrderedItem item) {
        this.item = item;
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public KioskUser getNextUpdateBy() {
        return nextUpdateBy;
    }

    public void setNextUpdateBy(KioskUser nextUpdateBy) {
        this.nextUpdateBy = nextUpdateBy;
    }

    public Status getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Status orderStatus) {
        this.orderStatus = orderStatus;
    }

}

/**
*Class Name: Settlement
*Project Name: KioskEJBModule
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Jan 19, 2017 10:05:22 AM
*(C)Morph Innovations Limited 2017. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.persistence.entity.payment;

import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.BusinessAccount;
import com.morph.kiosk.persistence.entity.KioskUser;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Entity
public class Settlement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private Business business;
    private Double amountBeforeSplit = 0.0;
    private Double totalSplitAmount = 0.0;
    private Double amountAfterSplit = 0.0;
    private Boolean paid;
    @ManyToOne
    private KioskUser settledBy;
    
    @ManyToOne
    private KioskUser paidBy;
    
    @ManyToOne
    private BusinessAccount businessAccount;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDate;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date settledDate;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date paymentDate;
    
    private String paymentCode;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public Double getAmountBeforeSplit() {
        return amountBeforeSplit;
    }

    public void setAmountBeforeSplit(Double amountBeforeSplit) {
        this.amountBeforeSplit = amountBeforeSplit;
    }

    public Double getTotalSplitAmount() {
        return totalSplitAmount;
    }

    public void setTotalSplitAmount(Double totalSplitAmount) {
        this.totalSplitAmount = totalSplitAmount;
    }

    public Double getAmountAfterSplit() {
        return amountAfterSplit;
    }

    public void setAmountAfterSplit(Double amountAfterSplit) {
        this.amountAfterSplit = amountAfterSplit;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public KioskUser getSettledBy() {
        return settledBy;
    }

    public void setSettledBy(KioskUser settledBy) {
        this.settledBy = settledBy;
    }

    public KioskUser getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(KioskUser paidBy) {
        this.paidBy = paidBy;
    }

    

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getSettledDate() {
        return settledDate;
    }

    public void setSettledDate(Date settledDate) {
        this.settledDate = settledDate;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    

    public BusinessAccount getBusinessAccount() {
        return businessAccount;
    }

    public void setBusinessAccount(BusinessAccount businessAccount) {
        this.businessAccount = businessAccount;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Settlement)) {
            return false;
        }
        Settlement other = (Settlement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.order.Settlement[ id=" + id + " ]";
    }

}

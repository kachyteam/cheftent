/**
*Class Name: DistanceCost
*Project Name: KioskEJBModule
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Mar 16, 2017 11:07:52 AM
*(C)Morph Innovations Limited 2017. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.persistence.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Entity
public class DistanceCost implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(nullable = false)
    private Double distance = 0.0;
    
    @Column(nullable = false)
    private Double cost = 0.0;
    
    @ManyToOne
    private Kiosk kiosk;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DistanceCost)) {
            return false;
        }
        DistanceCost other = (DistanceCost) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.DistanceCost[ id=" + id + " ]";
    }

}

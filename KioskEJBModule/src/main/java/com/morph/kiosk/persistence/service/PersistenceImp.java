/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author root
 */
@Named
public class PersistenceImp implements Persistence, Serializable{

    //@Produces
    private EntityManager em;

    @Override
    public <T> T create(T t) {
        em.persist(t);
        em.flush();
        return t;
    }

    @Override
    public <T> T find(Class<T> type, Object id) {
        return em.find(type, id);
    }

    @Override
    public <T> void delete(T t) {
        em.remove(em.merge(t));
    }

    @Override
    public <T> T update(T t) {
        return em.merge(t);
    }

    @Override
    public List findWithNamedQuery(String queryName) {
        return em.createNamedQuery(queryName).getResultList();
    }

    @Override
    public List findWithNamedQuery(String queryName, int resultLimit) {
        return em.createNamedQuery(queryName).setMaxResults(resultLimit).getResultList();
    }

    @Override
    public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
        return findWithNamedQuery(namedQueryName, parameters, 0);
    }

    @Override
    public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit) {
        Query query = this.em.createNamedQuery(namedQueryName);
        if (resultLimit > 0) {
            query.setMaxResults(resultLimit);
        }
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }

    @Override
    public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int first, int resultLimit) {
        Query query = this.em.createNamedQuery(namedQueryName);
        if (resultLimit > 0) {
            
            query.setFirstResult(first).setMaxResults(resultLimit);
        }
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }

    @Override
    public <T> List<T> findWithNativeQuery(String sql) {
        return em.createNativeQuery(sql).getResultList();
    }

    @Override
    public <T> T findSingleWithNativeQuery(String sql) {
        return (T) em.createNativeQuery(sql).getSingleResult();
    }

    @Override
    public <T> List<T> findWithNativeQuery(String sql, int resultLimit) {
        return em.createNativeQuery(sql).setMaxResults(resultLimit).getResultList();
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public <T> List<T> findWithQuery(String queryName, int startAt, int resultLimit) {
        return em.createQuery(queryName).setFirstResult(startAt).setMaxResults(resultLimit).getResultList();
    }

    public <T> List<T> findWithQuery(String queryName) {
        return em.createQuery(queryName).getResultList();
    }

    public <T> T findSingleWithQuery(String query) {
        try {
            return (T) em.createQuery(query).getSingleResult();
        } catch (NoResultException nre) {
            System.err.println("Error ::: " + nre);
            return null;
        } catch (Exception e) {
            System.err.println("Error ::: " + e);
            return null;
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.service;

import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.portal.PortalUser;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.portal.BusinessStaff;
import com.morph.kiosk.persistence.entity.portal.BusinessStaffRole;
import com.morph.kiosk.persistence.entity.portal.PortalPermission;
import com.morph.kiosk.persistence.entity.portal.PortalSuperAdminUser;
import com.morph.kiosk.persistence.entity.portal.PortalUserAccessGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_AccessGroup;
import com.morph.kiosk.persistence.entity.portal.PortalUserGroup_User;
import com.morph.util.PersistenceConstantUtil;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Tobi-Morph-PC
 */
//@Named
@Stateless
public class UserPersistence extends PersistenceImp {

    /**
     *
     * @param em
     */
    @Override
    @PersistenceContext(unitName = "KioskEJBModule_V3_PU")
    public void setEm(EntityManager em) {
        super.setEm(em);
    }

    public KioskUser getKioskUserByEmailAddress(String emailAddress) {
        KioskUser user;
        try {
            user = getEm().createQuery("SELECT o FROM KioskUser o WHERE o.emailAddress =:emailAddress", KioskUser.class)
                    .setParameter("emailAddress", emailAddress)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            user = null;
        }
        return user;
    }

    public KioskUser getKioskUserByEmailAddressAndPassword(String email, String password) {
        KioskUser user;
        try {
            user = (KioskUser) getEm().createQuery("SELECT O FROM KioskUser O WHERE O.emailAddress =:emailAddress AND O.password =:password AND O.active = TRUE")
                    .setParameter("emailAddress", email)
                    .setParameter("password", password)
                    .getSingleResult();
        } catch (Throwable t) {
            t.getLocalizedMessage();
            user = null;
        }
        return user;
    }

    public PortalUser getPortalUserByKioskUser(Long id) {
        PortalUser user;
        try {
            user = (PortalUser) getEm().createQuery("SELECT O FROM PortalUser O WHERE O.user.id =:id")
                    .setParameter("id", id).getSingleResult();
        } catch (Throwable t) {
            t.getLocalizedMessage();
            user = null;
        }
        return user;
    }

    public List<KioskUser> getAllActiveKioskUsers() {
        try {
            return getEm().createQuery("SELECT o FROM KioskUser o WHERE o.active =1", KioskUser.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Long getAllActiveKioskUsersCount() {
        return getEm().createQuery("SELECT COUNT(o) FROM KioskUser o WHERE o.active =1", Long.class).getSingleResult();
    }

    public List<KioskUser> getAllDeactivatedKioskUsers() {
        try {
            return getEm().createQuery("SELECT o FROM KioskUser o WHERE o.active =0", KioskUser.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Long getAllDeactivatedKioskUsersCount() {
        return getEm().createQuery("SELECT COUNT(o) FROM KioskUser o WHERE o.active =0", Long.class).getSingleResult();
    }

    public List<PortalUser> getAllActivePortalUsers() {
        try {
            return getEm().createQuery("SELECT o FROM PortalUser o WHERE o.active =1", PortalUser.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Long getAllActivePortalUsersCount() {
        return getEm().createQuery("SELECT o FROM PortalUser o WHERE o.active =1", Long.class).getSingleResult();
    }

    public List<PortalUser> getAllDeactivatedPortalUsers() {
        try {
            return getEm().createQuery("SELECT o FROM PortalUser o WHERE o.active =0", PortalUser.class).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public Long getAllDeactivatedPortalUsersCount() {
        return getEm().createQuery("SELECT o FROM PortalUser o WHERE o.active =0", Long.class).getSingleResult();
    }

    public List<PortalUserAccessGroup> getPortalUserAccessGroupByUser(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM PortalUserAccessGroup o WHERE o.user.id=:id", PortalUserAccessGroup.class)
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }

    }

    public KioskUser getUserByPhoneNo(String phoneNo) {
        KioskUser user;
        try {
            //TypedQuery query = Query("")
            user = getEm().createQuery("SELECT o FROM KioskUser o WHERE o.phoneNumber =:phoneNo", KioskUser.class)
                    .setParameter("phoneNo", phoneNo)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            user = null;
        }
        return user;
    }

    public KioskUser getKioskUserByEmailOrPhoneNumber(String emailAddress, String phoneNumber) {
        KioskUser user;
        try {
            user = getEm().createQuery("SELECT o FROM KioskUser o WHERE o.emailAddress=:emailAddress OR o.phoneNumber=:phoneNumber", KioskUser.class)
                    .setParameter("emailAddress", emailAddress)
                    .setParameter("phoneNumber", phoneNumber)
                    .getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            user = null;
        }
        return user;
    }

    public Business getBusinessByUserIdAndBusinessId(Long id, Long bquery) {
        Business b;
        try {
            b = getEm().createQuery("SELECT o FROM Business o WHERE o.owner.id=:id AND o.id=:bquery", Business.class)
                    .setParameter("id", id).setParameter("bquery", bquery).getSingleResult();
        } catch (Exception e) {
            e.getLocalizedMessage();
            b = null;
        }
        return b;
    }

    public List<KioskUser> getBusinessStaffListByBusinessOwner(Long id) {
        try {
            return getEm().createQuery("SELECT o.staff FROM BusinessStaff o WHERE o.creator.id=:id")
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            e.getLocalizedMessage();
            return Collections.EMPTY_LIST;
        }
    }

    public PortalUserGroup_User getUserGroupByPortalUser(Long id) {
        PortalUserGroup_User pp;
        try {
            pp = (PortalUserGroup_User) getEm().createQuery("SELECT o FROM PortalUserGroup_User o WHERE o.user.id=:id")
                    .setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            pp = null;
            System.err.println("getUserGroupByPortalUser::: " + e);
        }
        return pp;
    }

    public PortalUserGroup_User getPortalUserGroup_UserByUserId(Long id) {
        PortalUserGroup_User pugu;
        try {
            pugu = getEm().createQuery("SELECT o FROM PortalUserGroup_User o WHERE o.user.id=:id", PortalUserGroup_User.class).setParameter("id", id).getSingleResult();
        } catch (NoResultException e) {
            System.err.println("getPortalUserGroup_UserByUserId ::: " + e.getMessage());
            pugu = null;
        }
        return pugu;
    }

    public List<PortalUserGroup_AccessGroup> getUserGroupAccessGroupByUserGroup(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM PortalUserGroup_AccessGroup o WHERE o.userGroup.id=:id")
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            System.err.println("getUserGroupAccessGroupByUserGroup :::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalPermission> getPortalPermissionByAccessGroup(Long id) {
        try {
            return getEm().createQuery("SELECT o.permission FROM PortalAccessGroup_Permission o WHERE o.accessGroup.id=:id ORDER BY o.permission.page.position DESC")
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            System.err.println("getPortalPermissionByAccessGroup :::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }
//    
//    public List<String> getPortalPermissionByAccessGroup(Long id) {
//        try {
//            return getEm().createQuery("SELECT o.permission.permissionName FROM PortalAccessGroup_Permission o WHERE o.accessGroup.id=:id")
//                    .setParameter("id", id).getResultList();
//        } catch (Exception e) {
//            System.err.println("getPortalPermissionByAccessGroup :::: " + e.getLocalizedMessage());
//            return Collections.EMPTY_LIST;
//        }
//    }

    public List<PortalUserGroup_AccessGroup> getUserGroupAccessGroupByAccessGroup(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM PortalUserGroup_AccessGroup o WHERE o.accessGroup.id=:id")
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            System.err.println("getUserGroupAccessGroupByAccessGroup :::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<PortalUserGroup_User> getPortalUserGroupByUserGroup(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM PortalUserGroup_User o WHERE o.userGroup.id=:id")
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            System.err.println("getPortalUserGroupByUserGroup :::: " + e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public List<BusinessStaff> getAllBusinessStaffByBusinessOnwer(Long id) {
        try {
            return getEm().createQuery("SELECT o FROM BusinessStaff o WHERE o.creator.id=:id", BusinessStaff.class)
                    .setParameter("id", id).getResultList();
        } catch (Exception e) {
            System.err.println("getAllBusinessStaffByBusinessOnwer Error ::: " + e.getMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public BusinessStaff getBusinessStaffByKioskUser(Long id) {
        BusinessStaff bs;
        try {
            bs = getEm().createQuery("SELECT o FROM BusinessStaff o WHERE o.staff.id=:id", BusinessStaff.class)
                    .setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.err.println("getBusinessStaffByKioskUser :::: " + e.getMessage());
            bs = null;
        }
        return bs;
    }

    public BusinessStaff getActiveBusinessStaffByKioskUser(Long id) {
        BusinessStaff bs;
        try {
            bs = getEm().createQuery("SELECT o FROM BusinessStaff o WHERE o.staff.id=:id AND o.active=1", BusinessStaff.class)
                    .setParameter("id", id).getSingleResult();
        } catch (Exception e) {
            System.err.println("getBusinessStaffByKioskUser :::: " + e.getMessage());
            bs = null;
        }
        return bs;
    }

    public List<BusinessStaffRole> getBusinessStaffRoleByBusinessStaff(Long businessStaffId) {
        try {
            return getEm().createQuery("SELECT o FROM BusinessStaffRole o WHERE o.businessStaff.id=:staffId", BusinessStaffRole.class)
                    .setParameter("staffId", businessStaffId)
                    .getResultList();
        } catch (Exception e) {
            System.err.println("getBusinessStaffRoleByBusinessStaff Error ::: " + e.getMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public BusinessStaffRole getBusinessStaffByStaffAndKiosk(Long staffId, Long kioskId) {
        BusinessStaffRole bsr;
        try {
            bsr = getEm().createQuery("SELECT o FROM BusinessStaffRole o WHERE o.businessStaff.id=:staffId AND o.kiosk.id=:kioskId", BusinessStaffRole.class)
                    .setParameter("staffId", staffId).setParameter("kioskId", kioskId)
                    .getSingleResult();
        } catch (Exception e) {
            bsr = null;
            System.err.println("getBusinessStaffByStaffAndKiosk Error ::::: " + e.getLocalizedMessage());
        }
        return bsr;
    }

    public BusinessStaffRole getBusinessStaffByStaffAndKiosk(Long staffId, Long kioskId, Long userGroupId) {
        BusinessStaffRole bsr;
        try {
            bsr = getEm().createQuery("SELECT o FROM BusinessStaffRole o WHERE o.businessStaff.id=:staffId AND o.kiosk.id=:kioskId AND o.userGroup.id=:userGroupId", BusinessStaffRole.class)
                    .setParameter("staffId", staffId)
                    .setParameter("kioskId", kioskId)
                    .setParameter("userGroupId", userGroupId)
                    .getSingleResult();
        } catch (Exception e) {
            bsr = null;
            System.err.println("getBusinessStaffByStaffAndKiosk Error ::::: " + e.getLocalizedMessage());
        }
        return bsr;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public PortalSuperAdminUser getSuperAdminUserByPortalUser(Long userId) {
        PortalSuperAdminUser superAdminUser;
        try {
            superAdminUser = getEm().createQuery("SELECT o FROM PortalSuperAdminUser o WHERE o.user.id=:id", PortalSuperAdminUser.class)
                    .setParameter("id", userId).getSingleResult();
        } catch (Exception e) {
            superAdminUser = null;
        }
        return superAdminUser;
    }

    public String hashPassword(String password) {
        String saltedPassword = PersistenceConstantUtil.SALT + password;
        String hashedPassword = PersistenceConstantUtil.generateHash(saltedPassword);
        return hashedPassword;
    }

    public Boolean checkHashedPassword(String enteredPassword, String storedPassword) {
        Boolean isAuthenticated;

        // remember to use the same SALT value use used while storing password
        // for the first time.
        String saltedPassword = PersistenceConstantUtil.SALT + enteredPassword;
        String hashedPassword = PersistenceConstantUtil.generateHash(saltedPassword);

        isAuthenticated = hashedPassword.equals(storedPassword);
        return isAuthenticated;
    }

    public KioskUser login(String email, String password) {
        KioskUser u = null;

        KioskUser ku = getKioskUserByEmailAddress(email);
        if (ku == null) {
            return u;
        }

        Boolean checkPassword = checkHashedPassword(password, ku.getPassword());
        if (checkPassword) {
            u = ku;
        }

        return u;
    }

}

/**
 * Class Name: OrderItem Project Name: KioskEJBModuleV2 Developer: Onyedika
 * Okafor (ookafor@morphinnovations.com) Version Info: Create Date: Aug 22, 2016
 * 3:52:25 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk.persistence.entity.order;

import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.order.KioskPaymentOption.Option;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com) Edit: Changed class
 * name to from orderItem to orderedItem to reflect the intent of the class
 * Added Cart and remove the purchase items list removed orderedBy as the cart
 * already have this property removed the kiosk column as the cart have this
 * property
 */
@Entity
public class OrderedItem implements Serializable {

//    @OneToMany(mappedBy = "orderItem", fetch = FetchType.EAGER)
//    private List<PurchasedItem> purchasedItems;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Cart cart;

    @Column(unique = true)
    private String batchId;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDate;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date checkedOutDate;

    @ManyToOne
    private KioskUser orderedBy;
    //Edit
    @ManyToOne
    private KioskDeliveryOption deliveryOption;

    private Double totalAmount = 0.0;

    private Double tax = 0.0;

    private Double deliveryFee = 0.0;
    
    private Double promotionDiscount = 0.0;
    
    private String promoCode;

    @Enumerated(EnumType.STRING)
    private Currency currency;

    @Enumerated(EnumType.STRING)
    private Option paymentMethod;

    @Enumerated(EnumType.STRING)
    private DeliveryTime deliveryTime;

    @ManyToOne
    private DeliveryAddress deliveryAddress;

    //Edited to kioskpaymentOption
    @ManyToOne
    private KioskPaymentOption paymentOption;

    @ManyToOne
    private Kiosk kiosk;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @ManyToOne
    private KioskUser lastModifiedBy;

    @Enumerated(EnumType.STRING)
    private Status orderStatus;

    private Long scheduledTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderedItem)) {
            return false;
        }
        OrderedItem other = (OrderedItem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entities.item.OrderItem[ id=" + id + " ]";
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCheckedOutDate() {
        return checkedOutDate;
    }

    public void setCheckedOutDate(Date checkedOutDate) {
        this.checkedOutDate = checkedOutDate;
    }

    public KioskDeliveryOption getDeliveryOption() {
        return deliveryOption;
    }

    public void setDeliveryOption(KioskDeliveryOption deliveryOption) {
        this.deliveryOption = deliveryOption;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public DeliveryAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public KioskPaymentOption getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(KioskPaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public KioskUser getOrderedBy() {
        return orderedBy;
    }

    public void setOrderedBy(KioskUser orderedBy) {
        this.orderedBy = orderedBy;
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }

    public Status getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Status orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public KioskUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(KioskUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public DeliveryTime getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(DeliveryTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Long getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(Long scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public Option getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Option paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(Double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public Double getPromotionDiscount() {
        return promotionDiscount;
    }

    public void setPromotionDiscount(Double promotionDiscount) {
        this.promotionDiscount = promotionDiscount;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }
    
    
    
    

    public enum Currency {

        Naira("NGN");

        private final String currency;

        private Currency(String currency) {
            this.currency = currency;
        }

        public String getCurrency() {
            return currency;
        }
    }

    public enum Status {

        PENDINGPAYMENT("Payment Pending"),
        INCOMPLETE("Incomplete"),
        CANCELLED("Cancelled"),
        PENDING("Pending"),
        RECEIVED("Received"),
        PROCESSING("Processing"),
        SHIPPED("Shipped"),
        // PARTIALDELIVERY("Partial Delivery"),
        DELIVERED("Delivered");

        private final String status;

        private Status(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }

    public enum DeliveryTime {

        ASAP("ASAP"),
        SCHEDULED("Scheduled");

        private final String deliveryTime;

        private DeliveryTime(String deliveryTime) {
            this.deliveryTime = deliveryTime;
        }

        public String getDeliveryTime() {
            return deliveryTime;
        }
    }

}

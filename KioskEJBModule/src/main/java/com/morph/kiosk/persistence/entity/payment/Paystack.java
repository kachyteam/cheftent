/**
*Class Name: Paystack
*Project Name: KioskEJBModule
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Oct 12, 2016 3:16:19 PM
*(C)Morph Innovations Limited 2016. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.persistence.entity.payment;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Entity
public class Paystack implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    /**
     * @param Name of the Provider Owner
     * publicKey -- params provided by the provider
     * secretKey -- params provided by the provider
     * status -- check if the paystack account is still active
     */
    private String name;
    private String publicKey;
    private String secretKey;
    private boolean status;
    
    
    @Column(unique = true, nullable = false)
    private String ownerEmailAddress;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paystack)) {
            return false;
        }
        Paystack other = (Paystack) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.payment.Paystack[ id=" + id + " ]";
    }

    public String getOwnerEmailAddress() {
        return ownerEmailAddress;
    }

    public void setOwnerEmailAddress(String ownerEmailAddress) {
        this.ownerEmailAddress = ownerEmailAddress;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.portal;

import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.Kiosk;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Tobi-Morph-PC
 */
@Entity
//@Table(uniqueConstraints = {@UniqueConstraint(columnNames={"businessStaff_id", "kiosk_id"})})
public class BusinessStaffRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private BusinessStaff businessStaff;
    
    @ManyToOne
    private Business business;
    
    @ManyToOne
    private Kiosk kiosk;
    
    @ManyToOne
    private PortalUserGroup userGroup;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateLastModified;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;
    
    private Boolean active;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BusinessStaffRole)) {
            return false;
        }
        BusinessStaffRole other = (BusinessStaffRole) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.portal.BusinessStaffRole[ id=" + id + " ]";
    }

    

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public Kiosk getKiosk() {
        return kiosk;
    }

    public void setKiosk(Kiosk kiosk) {
        this.kiosk = kiosk;
    }

    public Date getDateLastModified() {
        return dateLastModified;
    }

    public void setDateLastModified(Date dateLastModified) {
        this.dateLastModified = dateLastModified;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public PortalUserGroup getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(PortalUserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public BusinessStaff getBusinessStaff() {
        return businessStaff;
    }

    public void setBusinessStaff(BusinessStaff businessStaff) {
        this.businessStaff = businessStaff;
    }
    
}

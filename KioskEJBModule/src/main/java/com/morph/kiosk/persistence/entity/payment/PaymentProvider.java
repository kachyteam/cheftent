/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.payment;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Tobi-Morph-PC
 * 
 * This class holds informations about the all the different providers being provided by the portal platform.
 * @see Paystack
 * @see Interswitch
 * @see MorphExpress
 * This class is an mediates between the provide and the portal user account.
 * The process works in the following way....
 * 1. All provider are register with this class.
 * 2. To get portal user account provider, get
 * @see UserPaymentProvider using the portal business id and this will process access to <b>PaymentProvider</b> from where you can dynamically handle the provider's payment process
 * with the <b>className</b>
 * 
 */
@Entity
public class PaymentProvider implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * the name of the provider
     */
    private String name;
    /**
     * description of the provider
     */
    private String description;
    /**
     * the Date the provider was added to the portal
     */
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDate;
    
    /**
     * the Date the provider was last modified to the portal
     */
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedDate;
    
    /**
     * status checks if the provider is still active on the portal platform
     */
    private boolean status;
    
    /**
     * this hold the class name of the provider. for example
     * @see Paystack
     * @see Interswitch
     * 
     * this variable will be use to get provider
     * 
     */
    private String className;
    //selected platform id
    /**
     * ProviderId is a backup if we later choose to move ahead with individual business having their own provider as oppose to the current implement of having only morph
     * remitting payment.
     * ProviderId is the database id of <b>PaymentProvider</b>
     * 
     */
    private Long providerId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentProvider)) {
            return false;
        }
        PaymentProvider other = (PaymentProvider) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entities.payment.PaymentProvider[ id=" + id + " ]";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    
    
}

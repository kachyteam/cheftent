/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.portal;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Tobi-Morph-PC
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames={"role_id", "accessGroup_id"})})
public class PortalAccessGroupPortalRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @OneToOne(fetch = FetchType.EAGER)
    private PortalAccessGroup accessGroup;
    
    @OneToOne(fetch = FetchType.EAGER)
    private PortalRole role;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PortalAccessGroupPortalRole)) {
            return false;
        }
        PortalAccessGroupPortalRole other = (PortalAccessGroupPortalRole) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.portal.PortalAccessGroupPortalRole[ id=" + id + " ]";
    }

    public PortalAccessGroup getAccessGroup() {
        return accessGroup;
    }

    public void setAccessGroup(PortalAccessGroup accessGroup) {
        this.accessGroup = accessGroup;
    }

    public PortalRole getRole() {
        return role;
    }

    public void setRole(PortalRole role) {
        this.role = role;
    }
    
}

/**
*Class Name: ItemVariation
*Project Name: KioskEJBModule
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Nov 28, 2016 7:36:52 PM
*(C)Morph Innovations Limited 2016. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.persistence.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"item_id", "description"})})
public class ItemVariation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private Item item;
    
    private String description;
    
    private Double price = 0.0;
    
    private boolean active;

    public ItemVariation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemVariation)) {
            return false;
        }
        ItemVariation other = (ItemVariation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.ItemVariation[ id=" + id + " ]";
    }

}

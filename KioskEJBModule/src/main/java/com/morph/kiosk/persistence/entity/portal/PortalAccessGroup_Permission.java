/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.entity.portal;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Tobi-Morph-PC
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames={"permission_id", "accessGroup_id"})})
public class PortalAccessGroup_Permission implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne(optional = false)
    private PortalAccessGroup accessGroup;
    
    @ManyToOne(optional = false)
    private PortalPermission permission;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateCreated;
    
    private boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PortalAccessGroup_Permission)) {
            return false;
        }
        PortalAccessGroup_Permission other = (PortalAccessGroup_Permission) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.portal.PortalAccessGroup_Permission[ id=" + id + " ]";
    }

    public PortalAccessGroup getAccessGroup() {
        return accessGroup;
    }

    public void setAccessGroup(PortalAccessGroup accessGroup) {
        this.accessGroup = accessGroup;
    }

    public PortalPermission getPermission() {
        return permission;
    }

    public void setPermission(PortalPermission permission) {
        this.permission = permission;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.morph.kiosk.persistence.service;

import com.morph.kiosk.persistence.entity.order.Cart;
import com.morph.kiosk.persistence.entity.order.Coupon;
import com.morph.kiosk.persistence.entity.order.CouponLog;
import com.morph.kiosk.persistence.entity.order.OrderedItem;
import com.morph.kiosk.persistence.entity.order.OrderedItemStatusLog;
import com.morph.kiosk.persistence.entity.order.PaymentTransaction;
import com.morph.kiosk.persistence.entity.order.SubItemCart;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

/**
 *
 * @author Onyedika
 */
@Startup
@Singleton
@LocalBean
public class Timer {

    @Resource
    TimerService timerService;

    @EJB
    private PortalPersistence persistence;

    @PostConstruct
    public void init() {
        long duration = 86400000;
        timerService.createIntervalTimer(1000, duration, new TimerConfig());
    }

    @Timeout
    public void deleteUnfinishedOrders() {
        System.out.println("Clearing Database of Incomplete Orders");

        List<OrderedItem> items = persistence.getOrderedItemByStage(OrderedItem.Status.INCOMPLETE);
        items.addAll(persistence.getOrderedItemByStage(OrderedItem.Status.PENDINGPAYMENT));
        for (OrderedItem item : items) {
            if (item.getCreatedDate() != null) {
                Long createdTime = item.getCreatedDate().getTime();
                Long expirationTime = createdTime + 3600000;
                Long currentTime = new Date().getTime();

                if (currentTime > expirationTime) {
                    List<PaymentTransaction> pts = persistence.getPaymentTransactionsByOrder(item);
                    for (PaymentTransaction pt : pts) {
                        persistence.delete(pt);
                    }

                    List<OrderedItemStatusLog> logs = persistence.getOrderLog(item.getId());
                    for (OrderedItemStatusLog log : logs) {
                        persistence.delete(log);
                    }

                    Cart c = persistence.getCartByBatchId(item.getBatchId());
                    if (c != null) {
                        List<SubItemCart> carts = persistence.getAllSubItemCartByCartId(c.getId());
                        for (SubItemCart sic : carts) {
                            persistence.delete(sic);
                        }
                        persistence.delete(c);
                    }

                    if (item.getPromoCode() != null) {
                        Coupon cp = persistence.getCouponByCode(item.getPromoCode());
                        if (cp != null) {
                            CouponLog cl = persistence.getCouponLogByOrderIdAndCouponId(item.getId(), cp.getId());
                            persistence.delete(cl);
                        }
                    }
                    persistence.delete(item);
                }
            } else {
                List<PaymentTransaction> pts = persistence.getPaymentTransactionsByOrder(item);
                for (PaymentTransaction pt : pts) {
                    persistence.delete(pt);
                }

                List<OrderedItemStatusLog> logs = persistence.getOrderLog(item.getId());
                for (OrderedItemStatusLog log : logs) {
                    persistence.delete(log);
                }

                Cart c = persistence.getCartByBatchId(item.getBatchId());
                if (c != null) {
                    List<SubItemCart> carts = persistence.getAllSubItemCartByCartId(c.getId());
                    for (SubItemCart sic : carts) {
                        persistence.delete(sic);
                    }
                    persistence.delete(c);
                }

                persistence.delete(item);
            }
        }
    }

}

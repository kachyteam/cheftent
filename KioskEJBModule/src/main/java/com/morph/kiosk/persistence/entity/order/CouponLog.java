/**
*Class Name: CouponLog
*Project Name: KioskEJBModule
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Mar 13, 2017 4:01:11 PM
*(C)Morph Innovations Limited 2017. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.persistence.entity.order;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@Entity
public class CouponLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private Long orderId;
    
    private Long couponId;
    
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CouponLog)) {
            return false;
        }
        CouponLog other = (CouponLog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entity.order.CouponLog[ id=" + id + " ]";
    }

}

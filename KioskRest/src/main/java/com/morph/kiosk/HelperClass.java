/**
 * Class Name: HelperClass Project Name: Kiosk Developer: Onyedika Okafor
 * (ookafor@morphinnovations.com) Version Info: Create Date: Aug 28, 2016
 * 3:10:29 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.morph.kiosk.model.FinalOrder;
import com.morph.kiosk.model.Food;
import com.morph.kiosk.model.FoodVariation;
import com.morph.kiosk.model.InitializeResponse;
import com.morph.kiosk.model.ItemViewMode;
import com.morph.kiosk.model.KioskViewMode;
import com.morph.kiosk.model.OrderSummary;
import com.morph.kiosk.model.OrderTray;
import com.morph.kiosk.model.PaystackInitializeResponse;
import com.morph.kiosk.model.PaystackPaymentRequest;
import com.morph.kiosk.model.SubItemGroupViewMode;
import com.morph.kiosk.model.TrayItem;
import com.morph.kiosk.model.UserModel;
import com.morph.kiosk.persistence.entity.Business;
import com.morph.kiosk.persistence.entity.DistanceCost;
import com.morph.kiosk.persistence.entity.Item;
import com.morph.kiosk.persistence.entity.ItemImage;
import com.morph.kiosk.persistence.entity.ItemVariation;
import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.KioskItem;
import com.morph.kiosk.persistence.entity.KioskUser;
import com.morph.kiosk.persistence.entity.PushRegister;
import com.morph.kiosk.persistence.entity.Settings;
import com.morph.kiosk.persistence.entity.SubItem;
import com.morph.kiosk.persistence.entity.SubItemGroup;
import com.morph.kiosk.persistence.entity.VariationSubItemCost;
import com.morph.kiosk.persistence.entity.order.Cart;
import com.morph.kiosk.persistence.entity.order.Coupon;
import com.morph.kiosk.persistence.entity.order.CouponLog;
import com.morph.kiosk.persistence.entity.order.DeliveryAddress;
import com.morph.kiosk.persistence.entity.order.KioskDeliveryOption;
import com.morph.kiosk.persistence.entity.order.KioskPaymentOption;
import com.morph.kiosk.persistence.entity.order.OrderedItem;
import com.morph.kiosk.persistence.entity.order.OrderedItemStatusLog;
import com.morph.kiosk.persistence.entity.order.PaymentTransaction;
import com.morph.kiosk.persistence.entity.order.SubItemCart;
import com.morph.kiosk.persistence.entity.payment.PaymentProvider;
import com.morph.kiosk.persistence.entity.payment.Paystack;
import com.morph.kiosk.persistence.entity.payment.Refund;
import com.morph.kiosk.persistence.entity.portal.BusinessStaffRole;
import com.morph.kiosk.persistence.service.PortalPersistence;
import com.morph.kiosk.persistence.service.UserPersistence;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import de.bytefish.fcmjava.client.FcmClient;
import de.bytefish.fcmjava.http.options.IFcmClientSettings;
import de.bytefish.fcmjava.model.enums.PriorityEnum;
import de.bytefish.fcmjava.model.options.FcmMessageOptions;
import de.bytefish.fcmjava.requests.data.DataMulticastMessage;
import de.bytefish.fcmjava.requests.notification.NotificationPayload;
import de.bytefish.fcmjava.responses.FcmMessageResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@RequestScoped
public class HelperClass {

    @Resource(lookup = "java:global/KioskEJBModule-1.2/PortalPersistence!com.morph.kiosk.persistence.service.PortalPersistence")
    PortalPersistence persistence;

    @Resource(lookup = "java:global/KioskEJBModule-1.2/UserPersistence!com.morph.kiosk.persistence.service.UserPersistence")
    UserPersistence userPersistence;

    @Inject
    HttpServletRequest request;

    public HelperClass() {
    }

    public OrderSummary processOrder(OrderTray tray) {
        OrderedItem oi = persistence.find(OrderedItem.class, tray.getId());
        OrderSummary orderSummary = new OrderSummary();
        oi.setCreatedDate(new Date());
        oi.setCurrency(OrderedItem.Currency.Naira);
        oi.setOrderStatus(OrderedItem.Status.INCOMPLETE);
        oi.setBatchId(oi.getId().toString());
        oi.setScheduledTime(tray.getDeliveryTime());

        if (tray.getAddress() != null) {
            if (tray.getAddress().getId() == null || tray.getAddress().getId() == 0) {
                DeliveryAddress address = new DeliveryAddress();
                LatLng latLng = getLatLng(tray.getAddress().getDescription());
                if (latLng != null) {
                    address.setLatitude(latLng.lat);
                    address.setLongitude(latLng.lng);
                }
                address.setDescription(tray.getAddress().getDescription());
                address.setLandmark(tray.getAddress().getLandmark());
                address.setPerson(tray.getAddress().getPerson());
                persistence.create(address);
                oi.setDeliveryAddress(address);
            } else {
                oi.setDeliveryAddress(persistence.find(DeliveryAddress.class, tray.getAddress().getId()));
            }
        }
//        else {
//            DeliveryAddress address = new DeliveryAddress();
//            persistence.create(address);
//            oi.setDeliveryAddress(address);
//        }

        if (tray.getDeliveryOption() != null) {
            oi.setDeliveryOption(persistence.find(KioskDeliveryOption.class, Long.valueOf(tray.getDeliveryOption())));
        }

        if (tray.getPaymentOption() != null) {
            oi.setPaymentOption(persistence.find(KioskPaymentOption.class, Long.valueOf(tray.getPaymentOption())));
            orderSummary.setPaymentOption(tray.getPaymentOption());
        }

        persistence.update(oi);
        Double subTotal = 0.0;
        //Added this to check carts
        List<Cart> cs = persistence.getAllCartsByBatchId(oi.getBatchId());
        List<SubItemCart> sscs = new ArrayList<>();
        for (Cart c : cs) {
            sscs.addAll(persistence.getAllSubItemCartByCartId(c.getId()));
        }

        for (SubItemCart sic : sscs) {
            persistence.delete(sic);
        }
        for (Cart c : cs) {
            persistence.delete(c);
        }

        for (TrayItem trayItem : tray.getItems()) {
            Cart cart = null;
            Double foodPrice = 0.0;
            Double subItemPrice = 0.0;
            KioskItem itemzz = persistence.find(KioskItem.class, trayItem.getFood().getId());
            oi.setKiosk(itemzz.getKiosk());
            if (trayItem.getVariation() == null) {
                KioskItem item = persistence.find(KioskItem.class, trayItem.getFood().getId());

                if (item != null) {
                    foodPrice = item.getItem().getPrice() * trayItem.getQuantity();

                    cart = new Cart();
                    cart.setAmount(foodPrice);
                    cart.setBatchId(oi.getBatchId());
                    cart.setCreatedDate(new Date());
                    cart.setItem(item.getItem());
                    cart.setMessage(trayItem.getInstruction());
                    cart.setType(Cart.ItemType.ITEM);
                    cart.setQuantity(Long.valueOf(trayItem.getQuantity()));
                    persistence.create(cart);
                }

            } else {
                ItemVariation iv = persistence.find(ItemVariation.class, trayItem.getVariation().getId());
                if (iv != null) {
                    foodPrice = iv.getPrice() * trayItem.getQuantity();

                    cart = new Cart();
                    cart.setAmount(foodPrice);
                    cart.setBatchId(oi.getBatchId());
                    cart.setCreatedDate(new Date());
                    cart.setItemVariation(iv);
                    cart.setMessage(trayItem.getInstruction());
                    cart.setType(Cart.ItemType.VARIATION);
                    cart.setQuantity(Long.valueOf(trayItem.getQuantity()));
                    persistence.create(cart);
                }

            }

            for (Food f : trayItem.getSubItems()) {
                SubItem si = persistence.find(SubItem.class, f.getId());
                if (si != null && cart != null) {
                    subItemPrice += f.getPrice();
                    SubItemCart sic = new SubItemCart();
                    sic.setCart(cart);
                    sic.setCreatedDate(new Date());
                    sic.setPrice(f.getPrice());
                    sic.setSubItem(si);
                    persistence.create(sic);
                }
            }

//Stay here for now 14-mar-17
//            for (Food f : trayItem.getSubItems()) {
//                SubItem si = persistence.find(SubItem.class, f.getId());
//                if (cart != null) {
//                    subItemPrice += f.getPrice();
//                    SubItemCart sic = new SubItemCart();
//                    sic.setCart(cart);
//                    sic.setCreatedDate(new Date());
//                    sic.setPrice(f.getPrice());
//                    sic.setSubItem(si);
//                    persistence.create(sic);
//                }
//            }
            subItemPrice = subItemPrice * trayItem.getQuantity();
            Double subsubTotal = foodPrice + subItemPrice;
            subTotal += subsubTotal;
        }

        Double deliveryFee = 0.0;
        if (oi.getDeliveryOption() != null) {
            if (oi.getDeliveryAddress() != null
                    && oi.getDeliveryOption().getDeliveryOption().equals(KioskDeliveryOption.Option.HOMEDELIVERY)) {

                // deliveryFee = oi.getDeliveryOption().getDeliveryCost();
                deliveryFee = calculateDeliveryCost(oi.getDeliveryAddress(), oi.getKiosk(), oi.getDeliveryOption());
            }
        }

        if (tray.getCoupon() != null) {
            Double discount = getCouponAmount(tray.getCoupon(), subTotal, oi);
            orderSummary.setDiscount(discount);
            orderSummary.setCouponCode(tray.getCoupon());
            oi.setPromoCode(tray.getCoupon());
            oi.setPromotionDiscount(discount);

            oi.setTotalAmount(getTotalAmount(discount, deliveryFee, 0.0, subTotal));
            //oi.setTotalAmount(getTotalAmount(discount, oi.getDeliveryOption().getDeliveryCost(), 0.0, subTotal));
        } else {
            oi.setTotalAmount(getTotalAmount(0.0, deliveryFee, 0.0, subTotal)); //discounted 
            //oi.setTotalAmount(getTotalAmount(0.0, oi.getDeliveryOption().getDeliveryCost(), 0.0, subTotal)); //discounted 
        }

        Double tax = (oi.getKiosk().getBusiness().getTaxRate().doubleValue() / 100) * oi.getTotalAmount();
        oi.setTotalAmount(tax + oi.getTotalAmount());// delivery fee has been calculated earlier

        oi.setTax(tax);
        oi.setDeliveryFee(deliveryFee);
        // oi.setDeliveryFee(oi.getDeliveryOption().getDeliveryCost());

        oi.setOrderStatus(OrderedItem.Status.PENDINGPAYMENT);
        persistence.update(oi);

        orderSummary.setOrderId(oi.getId());
        orderSummary.setSubtotal(subTotal);
        orderSummary.setDeliveryCost(deliveryFee);
        // orderSummary.setDeliveryCost(oi.getDeliveryOption().getDeliveryCost());
        orderSummary.setTax(tax);
        orderSummary.setTaxRate(oi.getKiosk().getBusiness().getTaxRate().doubleValue());
        orderSummary.setTotal(oi.getTotalAmount());

        return orderSummary;
    }

    private Double getCouponAmount(String couponCode, Double subTotal, OrderedItem oi) {
        Coupon c = persistence.getCouponByCode(couponCode);
        Double discount = 0.0;
        if (c != null) {
            if (c.getCouponBatch().getPercentage() > 0.0) {
                Double dis = (c.getCouponBatch().getPercentage() / 100.0) * subTotal;
                if (dis > c.getCouponBatch().getMaxAmount()) {
                    discount = c.getCouponBatch().getMaxAmount();
                } else {
                    discount = dis;
                }
            } else {
                discount = c.getCouponBatch().getCouponAmount();
            }

            CouponLog log = persistence.getCouponLogByOrderIdAndCouponId(oi.getId(), c.getId());
            if (log == null) {
                CouponLog l = new CouponLog();
                l.setCouponId(c.getId());
                l.setOrderId(oi.getId());
                persistence.create(l);
            }
        }
        return discount;
    }

    public Double getTotalAmount(Double coupon, Double deliveryCost, Double tax, Double subTotal) {
        Double totalAmount = (subTotal + deliveryCost + tax) - coupon;
        return totalAmount;
    }

    private Double paystackCharge(Double amount) {
        Double charge;
        if (amount < 2500) {
            Settings s = persistence.getSettingByName("Paystack Percentage Charge");
            Double percentCharge = Double.valueOf(s.getSettingValue());
            charge = (percentCharge / 100) * amount;
        } else {
            Settings s1 = persistence.getSettingByName("Paystack Flat Charge");
            Double flatCharge = Double.valueOf(s1.getSettingValue());

            Settings s = persistence.getSettingByName("Paystack Percentage Charge");
            Double percentCharge = Double.valueOf(s.getSettingValue());

            charge = ((percentCharge / 100) * amount) + flatCharge;

        }
        return charge;
    }

    public InitializeResponse payStackPayment(InitializeResponse ir, OrderedItem oi, PaymentProvider pp) {
        PaystackPaymentRequest ppr = new PaystackPaymentRequest();
        Paystack paystack = persistence.find(Paystack.class, pp.getProviderId());
        ppr.setAmount(Long.valueOf(String.valueOf(oi.getTotalAmount().intValue()) + "00"));
        if (oi.getDeliveryAddress() != null) {
            if (oi.getDeliveryAddress().getEmailAddress() != null) {
                ppr.setEmail(oi.getDeliveryAddress().getEmailAddress());
            } else {
                ppr.setEmail(paystack.getOwnerEmailAddress());
            }
        } else {
            ppr.setEmail(paystack.getOwnerEmailAddress());
        }

        ppr.setReference(UUID.randomUUID().toString());
        URI contextUrl = URI.create(request.getRequestURL().toString()).resolve(request.getContextPath());
        StringBuilder s = new StringBuilder(contextUrl.toString());
        s.append(ProjectConstants.PAYSTACK_RESPONSE_URL);
        //  ppr.setCallback_url("http://localhost:8080/Kiosk/service/paystackPostPaymentResponse");
        // ppr.setCallback_url("http://192.168.1.175:8080/Kiosk/service/paystackPostPaymentResponse");
        // ppr.setCallback_url("http://154.113.0.217:8080/Kiosk/service/paystackPostPaymentResponse");

        ppr.setCallback_url(s.toString());

        String secretKey = paystack.getSecretKey();
        PaystackInitializeResponse initializeResponse = initializePayment(ppr, secretKey);
        if (initializeResponse.isStatus()) {
            ir.setMessage(initializeResponse.getData().getAuthorization_url());
            ir.setResponseCode("01");

            Double paystackCharge = paystackCharge(oi.getTotalAmount());
            PaymentTransaction pt = new PaymentTransaction();
            pt.setAmount(new BigDecimal(oi.getTotalAmount() - paystackCharge).setScale(2, RoundingMode.HALF_UP).doubleValue());
            pt.setAmountBeforeCharge(new BigDecimal(oi.getTotalAmount()).setScale(2, RoundingMode.HALF_UP).doubleValue());
            pt.setPaymentPlatformCharge(new BigDecimal(paystackCharge).setScale(2, RoundingMode.HALF_UP).doubleValue());
            pt.setPayer(oi.getOrderedBy());
            pt.setPaymentProvider(pp);
            pt.setTractionStatus(PaymentTransaction.PaymentTractionStatus.PAYMENTPENDING);
            pt.setTransactionRef(ppr.getReference());
            pt.setOrderItem(oi);
            persistence.create(pt);
        } else {
            System.out.println("Payment Error");
            ir.setMessage(initializeResponse.getMessage());
            ir.setResponseCode("00");
        }
        return ir;
    }

    public PaystackInitializeResponse verifyPayment(String trxref, String secretKey) {
        Client c = Client.create();
        WebResource webResource = c.resource(ProjectConstants.PAYSTACK_VERIFY_PAYMENT_URL + trxref);
        ClientResponse response = webResource.header("authorization", "Bearer " + secretKey)
                .get(ClientResponse.class);
        return response.getEntity(PaystackInitializeResponse.class);
    }

    public PaystackInitializeResponse initializePayment(PaystackPaymentRequest ppr, String secretKey) {
        try {
            Client client = Client.create();
            WebResource webResource = client.resource(ProjectConstants.PAYSTACK_INITIALIZE_PAYMENT_URL);
            ClientResponse response = webResource.header("authorization", "Bearer " + secretKey)
                    .header("content-type", "application/json")
                    .type("application/json")
                    .post(ClientResponse.class, ppr);
            return response.getEntity(PaystackInitializeResponse.class);
        } catch (UniformInterfaceException | ClientHandlerException e) {
            e.printStackTrace();
            return new PaystackInitializeResponse();
        }
    }

    public void handleClientHandlerException(ClientHandlerException ex) throws ClientHandlerException {
        if (ex.getCause() instanceof SocketTimeoutException) {
            System.err.println("System Timed Out");
        }
        throw ex;
    }

    public Food kioskItemToFood(KioskItem ki) {
        Food itemViewMode = new Food();
        itemViewMode.setAvailable(ki.isStatus());
        itemViewMode.setDateAdded(ki.getDateAdded());
        itemViewMode.setId(ki.getId());
        itemViewMode.setName(ki.getItem().getName());
        itemViewMode.setDescription(ki.getItem().getDescription());
        itemViewMode.setPrice(ki.getItem().getPrice());
        ItemImage ii = persistence.getItemImage(ki.getItem());
        itemViewMode.setImageUrl(ii != null ? ii.getImageUrl() : "");
        itemViewMode.setCurrency(OrderedItem.Currency.Naira.getCurrency());

        List<ItemVariation> ivs = persistence.getActiveItemVariationsByItemId(ki.getItem().getId());
        List<FoodVariation> fvs = new ArrayList<>();

        if (ivs.isEmpty()) {
            List<SubItem> sis = persistence.getAllItemSubItems(ki.getItem().getId());
            List<ItemViewMode> ivms = new ArrayList<>();

            for (SubItem si : sis) {
                ItemViewMode ivm = new ItemViewMode();
                ivm.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                ivm.setName(si.getName());
                ivm.setPrice(si.getPrice());
                ivm.setId(si.getId());
                ivms.add(ivm);
            }
            itemViewMode.setSubItems(ivms);

            List<SubItemGroup> groups = persistence.getAllItemSubItemGroups(ki.getItem().getId());
            List<SubItemGroupViewMode> sigvm = new ArrayList<>();

            for (SubItemGroup sig : groups) {
                SubItemGroupViewMode viewMode = new SubItemGroupViewMode();
                viewMode.setName(sig.getName());
                List<SubItem> items = persistence.getSubItemsFromSubItemGroup(sig.getId());
                List<ItemViewMode> ivmss = new ArrayList<>();
                for (SubItem si : items) {
                    ItemViewMode ivm = new ItemViewMode();
                    ivm.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                    ivm.setName(si.getName());
                    ivm.setPrice(si.getPrice());
                    ivm.setId(si.getId());
                    ivmss.add(ivm);
                }
                viewMode.setSubItems(ivmss);
                sigvm.add(viewMode);
            }
            itemViewMode.setSubItemGroups(sigvm);
        }

        for (ItemVariation iv : ivs) {
            FoodVariation fv = new FoodVariation();
            fv.setCurrency(OrderedItem.Currency.Naira.getCurrency());
            fv.setDescription(iv.getDescription());
            fv.setPrice(iv.getPrice());
            fv.setId(iv.getId());

            List<SubItem> sis = persistence.getAllItemSubItems(ki.getItem().getId());
            List<ItemViewMode> ivms = new ArrayList<>();
            for (SubItem si : sis) {
                VariationSubItemCost cost = persistence.getVariationCostBySubItemAndVarition(si, iv);
                ItemViewMode ivm = new ItemViewMode();
                ivm.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                ivm.setName(si.getName());
                if (cost != null) {
                    ivm.setPrice(cost.getPrice());
                } else {
                    ivm.setPrice(si.getPrice());
                }
                ivm.setId(si.getId());
                ivms.add(ivm);
            }
            fv.setSubItems(ivms);

            List<SubItemGroup> groups = persistence.getAllItemSubItemGroups(ki.getItem().getId());
            List<SubItemGroupViewMode> sigvm = new ArrayList<>();

            for (SubItemGroup sig : groups) {
                SubItemGroupViewMode viewMode = new SubItemGroupViewMode();
                viewMode.setName(sig.getName());
                List<SubItem> items = persistence.getSubItemsFromSubItemGroup(sig.getId());
                List<ItemViewMode> ivmss = new ArrayList<>();
                for (SubItem si : items) {
                    VariationSubItemCost cost = persistence.getVariationCostBySubItemAndVarition(si, iv);
                    ItemViewMode ivm = new ItemViewMode();
                    ivm.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                    ivm.setName(si.getName());
                    if (cost != null) {
                        ivm.setPrice(cost.getPrice());
                    } else {
                        ivm.setPrice(si.getPrice());
                    }
                    ivm.setId(si.getId());
                    if (!sig.isMultiSelect()) {
                        ivmss.add(ivm);
                    } else {
                        ivms.add(ivm);
                    }
                }
                viewMode.setSubItems(ivmss);
                sigvm.add(viewMode);
            }
            fv.setSubItemGroups(sigvm);

            fvs.add(fv);
        }
        itemViewMode.setVariation(fvs);

        return itemViewMode;
    }

    public ItemViewMode kioskItemToItemViewMode(KioskItem ki) {
        ItemViewMode itemViewMode = new ItemViewMode();
        itemViewMode.setAvailable(ki.isStatus());
        itemViewMode.setDateAdded(ki.getDateAdded());
        itemViewMode.setId(ki.getId());
        itemViewMode.setName(ki.getItem().getName());
        itemViewMode.setDescription(ki.getItem().getDescription());
        itemViewMode.setPrice(ki.getItem().getPrice());
        ItemImage ii = persistence.getItemImage(ki.getItem());
        itemViewMode.setImageUrl(ii != null ? ii.getImageUrl() : "");
        itemViewMode.setCurrency(OrderedItem.Currency.Naira.getCurrency());

        List<ItemVariation> ivs = persistence.getActiveItemVariationsByItemId(ki.getItem().getId());
        List<FoodVariation> fvs = new ArrayList<>();

        if (ivs.isEmpty()) {
            List<SubItem> sis = persistence.getAllItemSubItems(ki.getItem().getId());
            List<ItemViewMode> ivms = new ArrayList<>();

            for (SubItem si : sis) {
                if (si.isActive()) {
                    ItemViewMode ivm = new ItemViewMode();
                    ivm.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                    ivm.setName(si.getName());
                    ivm.setPrice(si.getPrice());
                    ivm.setId(si.getId());
                    ivms.add(ivm);
                }
            }
            itemViewMode.setSubItems(ivms);

            List<SubItemGroup> groups = persistence.getAllItemSubItemGroups(ki.getItem().getId());
            List<SubItemGroupViewMode> sigvm = new ArrayList<>();

            for (SubItemGroup sig : groups) {
                if (sig.isActive()) {
                    SubItemGroupViewMode viewMode = new SubItemGroupViewMode();
                    viewMode.setName(sig.getName());
                    List<SubItem> items = persistence.getSubItemsFromSubItemGroup(sig.getId());
                    List<ItemViewMode> ivmss = new ArrayList<>();
                    for (SubItem si : items) {
                        ItemViewMode ivm = new ItemViewMode();
                        ivm.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                        ivm.setName(si.getName());
                        ivm.setPrice(si.getPrice());
                        ivm.setId(si.getId());
                        if (!sig.isMultiSelect()) {
                            ivmss.add(ivm);
                        } else {
                            ivms.add(ivm);
                        }
                    }
                    viewMode.setSubItems(ivmss);
                    sigvm.add(viewMode);
                }
            }
            itemViewMode.setSubItemGroups(sigvm);
        }

        for (ItemVariation iv : ivs) {
            FoodVariation fv = new FoodVariation();
            fv.setCurrency(OrderedItem.Currency.Naira.getCurrency());
            fv.setDescription(iv.getDescription());
            fv.setId(iv.getId());
            fv.setPrice(iv.getPrice());

            List<SubItem> sis = persistence.getAllItemSubItems(ki.getItem().getId());
            List<ItemViewMode> ivms = new ArrayList<>();
            for (SubItem si : sis) {
                VariationSubItemCost cost = persistence.getVariationCostBySubItemAndVarition(si, iv);
                ItemViewMode ivm = new ItemViewMode();
                ivm.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                ivm.setName(si.getName());
                if (cost != null) {
                    ivm.setPrice(cost.getPrice());
                } else {
                    ivm.setPrice(si.getPrice());
                }
                ivm.setId(si.getId());
                ivms.add(ivm);
            }
            fv.setSubItems(ivms);

            List<SubItemGroup> groups = persistence.getAllItemSubItemGroups(ki.getItem().getId());
            List<SubItemGroupViewMode> sigvm = new ArrayList<>();

            for (SubItemGroup sig : groups) {
                SubItemGroupViewMode viewMode = new SubItemGroupViewMode();
                viewMode.setName(sig.getName());
                List<SubItem> items = persistence.getSubItemsFromSubItemGroup(sig.getId());
                List<ItemViewMode> ivmss = new ArrayList<>();
                for (SubItem si : items) {
                    VariationSubItemCost cost = persistence.getVariationCostBySubItemAndVarition(si, iv);
                    ItemViewMode ivm = new ItemViewMode();
                    ivm.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                    ivm.setName(si.getName());
                    if (cost != null) {
                        ivm.setPrice(cost.getPrice());
                    } else {
                        ivm.setPrice(si.getPrice());
                    }
                    ivm.setId(si.getId());
                    if (!sig.isMultiSelect()) {
                        ivmss.add(ivm);
                    } else {
                        ivms.add(ivm);
                    }
                }
                viewMode.setSubItems(ivmss);
                sigvm.add(viewMode);
            }
            fv.setSubItemGroups(sigvm);

            fvs.add(fv);
        }
        itemViewMode.setVariation(fvs);

        return itemViewMode;
    }

    public ItemViewMode itemToItemViewMode(Item i) {
        ItemViewMode itemViewMode = new ItemViewMode();
        itemViewMode.setName(i.getName());

        itemViewMode.setAvailable(i.getAvailable());
        itemViewMode.setDateAdded(i.getCreatedDate());
        itemViewMode.setId(i.getId());
        itemViewMode.setDescription(i.getDescription());

        return itemViewMode;
    }

    public ItemViewMode subItemToItemViewMode(SubItem i) {
        ItemViewMode itemViewMode = new ItemViewMode();
        itemViewMode.setName(i.getName());
        itemViewMode.setPrice(i.getPrice());
        itemViewMode.setDateAdded(i.getCreatedDate());
        itemViewMode.setId(i.getId());

        return itemViewMode;
    }

    public KioskViewMode kioskToKioskViewMode(Kiosk k) {
        KioskViewMode kioskViewMode = new KioskViewMode();
        kioskViewMode.setDescription(k.getBusiness() != null ? k.getBusiness().getDescription() : null);
        kioskViewMode.setId(k.getId());
        kioskViewMode.setName(k.getName());
        kioskViewMode.setClosed(k.isClosed());
        kioskViewMode.setMinDeliveryTime(k.getMinDeliveryTime());
        kioskViewMode.setAllowSchedule(k.isAllowSchedule());
        kioskViewMode.setLogo(k.getBusiness() != null ? k.getBusiness().getLogoName() : null);

        return kioskViewMode;
    }

    public KioskViewMode businessToBusinessViewMode(Business b) {
        KioskViewMode kioskViewMode = new KioskViewMode();
        kioskViewMode.setDescription(b.getDescription());
        kioskViewMode.setId(b.getId());
        kioskViewMode.setName(b.getName());
        kioskViewMode.setLogo(b.getLogoName());

        return kioskViewMode;
    }

    public UserModel customer(KioskUser ku) {
        UserModel kc = new UserModel();
        if (ku != null) {
            kc.setEmailAddress(ku.getEmailAddress());
            kc.setFirstName(ku.getFirstName());
            kc.setId(ku.getId());
            kc.setMiddleName(ku.getMiddleName());
            kc.setPhoneNo(ku.getPhoneNumber());
            kc.setSurname(ku.getSurname());
        }
        return kc;
    }

    public FinalOrder finalOrderFromOrderItem(OrderedItem os) {
        FinalOrder fo = new FinalOrder();
        fo.setCheckedOutDate(os.getCheckedOutDate());
        //  fo.setCoupon(os.getPromotionDiscount());
        fo.setPromotionDiscount(os.getPromotionDiscount());
        fo.setCreatedDate(os.getCreatedDate());
        fo.setUpdatedDate(os.getLastModifiedDate());
        fo.setId(os.getId());
        fo.setDeliveryAddress(os.getDeliveryAddress());
        fo.setDeliveryOption(os.getDeliveryOption());
        fo.setCurrency(OrderedItem.Currency.Naira.getCurrency());
        fo.setKiosk(os.getKiosk() != null ? kioskToKioskViewMode(os.getKiosk()) : null);
        fo.setOrderedBy(customer(os.getOrderedBy()));
        fo.setUpdatedby(customer(os.getLastModifiedBy()));
        fo.setPaymentOption(os.getPaymentOption());
        fo.setTotalAmount(os.getTotalAmount());
        fo.setStatus(os.getOrderStatus() != null ? os.getOrderStatus().getStatus() : null);
        fo.setTax(os.getTax());
        fo.setTaxRate(os.getKiosk().getBusiness().getTaxRate().doubleValue());
        fo.setDeliveryFee(os.getDeliveryFee());
        PaymentTransaction pt = persistence.getPaymentTransactionByOrder(os);
        if (pt != null) {
            fo.setPaymentStatus(pt.getTractionStatus().getStatus());
        }

        List<Cart> carts = persistence.getAllCartsByBatchId(os.getId().toString());
        List<TrayItem> items = new ArrayList<>();
        for (Cart pi : carts) {
            TrayItem ti = new TrayItem();
            ti.setInstruction(pi.getMessage());
            ti.setQuantity(pi.getQuantity().intValue());
            List<SubItemCart> carts1 = persistence.getAllSubItemCartByCartId(pi.getId());

            if (pi.getType() != null) {
                if (pi.getType().equals(Cart.ItemType.VARIATION)) {
                    FoodVariation fv = new FoodVariation();
                    fv.setDescription(pi.getItemVariation().getDescription());
                    fv.setId(pi.getItemVariation().getId());
                    fv.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                    fv.setPrice(pi.getItemVariation().getPrice());
                    ti.setVariation(fv);

                    Food f = new Food();
                    f.setId(pi.getItemVariation().getItem().getId());
                    f.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                    f.setName(pi.getItemVariation().getItem().getName());
                    f.setPrice(pi.getItemVariation().getItem().getPrice());
                    ti.setFood(f);
                } else {
                    Food f = new Food();
                    f.setDescription(pi.getItem().getDescription());
                    f.setId(pi.getItem().getId());
                    f.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                    f.setName(pi.getItem().getName());
                    f.setPrice(f.getPrice());
                    ti.setFood(f);
                }
            }

            if (!carts1.isEmpty()) {
                List<Food> fs = new ArrayList<>();
                for (SubItemCart sic : carts1) {
                    Food f1 = new Food();
                    f1.setName(sic.getSubItem().getName());
                    f1.setCurrency(OrderedItem.Currency.Naira.getCurrency());
                    f1.setId(sic.getSubItem().getId());
                    // f1.setPrice(sic.getSubItem().getPrice());
                    f1.setPrice(sic.getPrice());
                    fs.add(f1);
                }
                ti.setSubItems(fs);
            }
            items.add(ti);
        }
        fo.setItems(items);

        return fo;
    }

    public void notify(List<KioskUser> users, String nextStatus) {
        List<String> customerRegisterId = new ArrayList<>();
        for (KioskUser ku : users) {
            PushRegister pr2 = persistence.getPushRegisterByKioskUser(ku);
            if (pr2 != null) {
                customerRegisterId.add(pr2.getRegId());
            }
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("command", ProjectConstants.SYNC_ORDER_TIMELINE);
        map.put("data", nextStatus);
        pushNotify(ProjectConstants.SYNC_ORDER_TIMELINE, customerRegisterId, null, map);
    }

    public void initiateSync(List<KioskUser> staff, String syncType) {
        List<String> staffGcmRegId = new ArrayList<>();
        for (KioskUser ku : staff) {
            PushRegister pr2 = persistence.getPushRegisterByKioskUser(ku);
            if (pr2 != null) {
                staffGcmRegId.add(pr2.getRegId());
            }
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("command", syncType);
        map.put("data", "Sync jobs");
        pushNotify(syncType, staffGcmRegId, null, map);
    }

    public void pushNotifyForPickUp(OrderedItem oi) {
        List<String> customerRegisterId = new ArrayList<>();
        PushRegister pr2 = persistence.getPushRegisterByKioskUser(oi.getOrderedBy());
        if (pr2 != null) {
            customerRegisterId.add(pr2.getRegId());
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("command", "pick_up_food");
        map.put("data", oi.getBatchId());
        pushNotify("pick_up_food", customerRegisterId, null, map);
    }

    public void pushNotifyForNewOrder(OrderedItem oi) {
        Kiosk k = oi.getKiosk();
        List<String> customerRegisterId = new ArrayList<>();

        PushRegister pr = persistence.getPushRegisterByKioskUser(k.getOwner());
        if (pr != null) {
            customerRegisterId.add(pr.getRegId());
        }
        List<KioskUser> kioskAttendants = persistence.getUserByKioskAndUsergroup(k, ProjectConstants.ATTENDANT);
        for (KioskUser ku : kioskAttendants) {
            PushRegister pr2 = persistence.getPushRegisterByKioskUser(ku);
            if (pr2 != null) {
                customerRegisterId.add(pr2.getRegId());
            }
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("command", "new_order");
        map.put("data", oi.getBatchId());
        pushNotify("new_order", customerRegisterId, null, map);
    }

    public void pushNotify(String collapseKey, List<String> recepientIds, NotificationPayload payload, HashMap<String, String> dataCommand) {
        try {
            FcmClient client = new FcmClient(new IFcmClientSettings() {
                @Override
                public String getFcmUrl() {
                    return ProjectConstants.FCM_URL;
                }

                @Override
                public String getApiKey() {
                    return ProjectConstants.PUSH_NOTIFICATION_API_KEY;
                }
            });

            FcmMessageOptions options = FcmMessageOptions.builder()
                    .setCollapseKey(collapseKey)
                    .setPriorityEnum(PriorityEnum.High)
                    .build();
            FcmMessageResponse response = client.send(new DataMulticastMessage(options, recepientIds, dataCommand, payload));
            ObjectMapper mapper = new ObjectMapper();

            String jsonInString;
            try {
                jsonInString = mapper.writeValueAsString(response);
                System.out.println(jsonInString);
            } catch (JsonProcessingException ex) {
                Logger.getLogger(HelperClass.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (Exception e) {
        }

    }

    /**
     * Returns true if the string is null or 0-length.
     *
     * @param str the string to be examined
     * @return true if str is null or zero length
     */
    public static boolean isEmpty(@Nullable CharSequence str) {
        if (str == null || str.length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static String generateToken() {
        UUID token = UUID.randomUUID();
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {

        }
        md.update(token.toString().getBytes());
        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

    public void notifications(OrderedItem.Status nextStatus, OrderedItem item, OrderedItemStatusLog log, Long recipient) {

        Set<KioskUser> kioskStaffs = new HashSet<>();
        kioskStaffs.add(item.getKiosk().getOwner());
        List<BusinessStaffRole> bsrs = persistence.getBusinessStaffRolesByKiosk(item.getKiosk());
        for (BusinessStaffRole bsr : bsrs) {
            if (!bsr.getUserGroup().getUserGroupName().equalsIgnoreCase(ProjectConstants.DISPATCH_RIDER)) {
                kioskStaffs.add(bsr.getBusinessStaff().getStaff());
            }
        }

        if (nextStatus.equals(OrderedItem.Status.SHIPPED)) {
            if (recipient != null) {
                KioskUser ku = persistence.find(KioskUser.class, recipient);
                log.setNextUpdateBy(ku);
                List<KioskUser> dispatchers = new ArrayList<>();
                dispatchers.add(ku);
                notify(dispatchers, nextStatus.getStatus());
                initiateSync(new ArrayList<>(kioskStaffs), ProjectConstants.SYNC_BACK_OFFICE);
                List<KioskUser> customer = new ArrayList<>();
                customer.add(item.getOrderedBy());
                notify(customer, nextStatus.getStatus());
            }
        }

        if (nextStatus == OrderedItem.Status.RECEIVED || nextStatus == OrderedItem.Status.PROCESSING) {
            List<KioskUser> customer = new ArrayList<>();
            customer.add(item.getOrderedBy());
            notify(customer, nextStatus.getStatus());
            initiateSync(new ArrayList<>(kioskStaffs), ProjectConstants.SYNC_BACK_OFFICE);
        }

        if (nextStatus == OrderedItem.Status.DELIVERED) {
            List<KioskUser> users = new ArrayList<>();
            users.add(item.getOrderedBy());
            users.add(item.getKiosk().getOwner());
            notify(users, nextStatus.getStatus());
        }
    }

    public Response cancelOrder(OrderedItem item, KioskUser modUser, String message) {
        item.setLastModifiedBy(modUser);
        item.setLastModifiedDate(new Date());
        item.setOrderStatus(OrderedItem.Status.CANCELLED);
        persistence.update(item);

        OrderedItemStatusLog log = new OrderedItemStatusLog();
        log.setComment(message);
        log.setCreatedDate(new Date());
        log.setItem(item);
        log.setKiosk(item.getKiosk());
        log.setLastModifiedDate(new Date());
        log.setOrderStatus(OrderedItem.Status.CANCELLED);
        log.setUpdatedby(modUser);
        persistence.create(log);

        Refund r = new Refund();
        r.setAmount(item.getTotalAmount());
        r.setBusiness(item.getKiosk().getBusiness());
        r.setCreatedDate(new Date());
        r.setOrderedItem(item);
        r.setPaid(false);
        persistence.create(r);

        FinalOrder fo = finalOrderFromOrderItem(item);

        List<KioskUser> users = new ArrayList<>();
        users.add(item.getOrderedBy());
        users.add(item.getKiosk().getOwner());
        notify(users, OrderedItem.Status.CANCELLED.getStatus());

        return Response.ok(fo).build();
    }

    public Response managePickup(OrderedItem item, KioskUser modUser, String message) {
        item.setLastModifiedBy(modUser);
        item.setLastModifiedDate(new Date());
        item.setOrderStatus(OrderedItem.Status.DELIVERED);
        persistence.update(item);

        OrderedItemStatusLog log = new OrderedItemStatusLog();
        log.setComment(message);
        log.setCreatedDate(new Date());
        log.setItem(item);
        log.setKiosk(item.getKiosk());
        log.setLastModifiedDate(new Date());
        log.setOrderStatus(OrderedItem.Status.DELIVERED);
        log.setUpdatedby(modUser);

        persistence.create(log);
        FinalOrder fo = finalOrderFromOrderItem(item);

        List<KioskUser> users = new ArrayList<>();
        users.add(item.getOrderedBy());
        users.add(item.getKiosk().getOwner());
        notify(users, OrderedItem.Status.DELIVERED.getStatus());
        pushNotifyForPickUp(item);

        return Response.ok(fo).build();
    }

    private Double calculateDeliveryCost(DeliveryAddress tAddress, Kiosk k, KioskDeliveryOption kdo) {
        Double cost = kdo.getDeliveryCost();
        if (kdo.isHasVariedCost()) {
            if (tAddress != null) {
                if (tAddress.getLatitude() != null && tAddress.getLongitude() != null) {
                    Double distance = persistence.distance(tAddress.getLatitude(), tAddress.getLongitude(), Double.parseDouble(k.getLatitude()), Double.parseDouble(k.getLongitude()), "K");
                    DistanceCost dc = persistence.getCostByDistanceAndKiosk(k, distance);
                    if (dc != null) {
                        cost = dc.getCost();
                    }
                }
            }
        }
        return cost;
    }

    private LatLng getLatLng(String address) {
        GeoApiContext context = new GeoApiContext().setApiKey("AIzaSyBBPFRD9W0ohojPZzSk-1fUiN3UvXaQTyQ");
        GeocodingResult[] results = null;
        try {
            results = GeocodingApi.geocode(context, address).await();
        } catch (Exception ex) {
            Logger.getLogger(KioskService.class.getName()).log(Level.SEVERE, null, ex);
        }
        LatLng latLng = results[0].geometry.location;

        return latLng;
    }
}

/**
 * Class Name: ApplicationConfig Project Name: Kiosk Developer: Onyedika Okafor
 * (ookafor@morphinnovations.com) Version Info: Create Date: Jun 23, 2016
 * 4:30:07 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk;

import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@ApplicationPath("service")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.morph.kiosk.JacksonConfig.class);
        resources.add(com.morph.kiosk.KioskService.class);
        resources.add(com.morph.kiosk.SupportService.class);
    }

}

/**
 * Class Name: ProjectConstants Project Name: Kiosk Developer: Onyedika Okafor
 * (ookafor@morphinnovations.com) Version Info: Create Date: Jul 22, 2016
 * 2:28:12 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk;


/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
public class ProjectConstants {

    //Error Names
    public static final String USER_FIRSTNAME_EMPTY = "USER_FIRSTNAME_EMPTY";
    public static final String USER_SURNAME_EMPTY = "USER_SURNAME_EMPTY";
    public static final String USER_PASSWORD_EMPTY = "USER_PASSWORD_EMPTY";
    public static final String USER_EMAILADDRESS_EMPTY = "USER_EMAILADDRESS_EMPTY";
    public static final String USER_PHONENO_EMPTY = "USER_PHONENO_EMPTY";
    public static final String USER_EMAILADDRESS_ALREADY_EXIST = "USER_EMAILADDRESS_ALREADY_EXIST";
    public static final String USER_PHONE_NUMBER_ALREADY_EXIST = "USER_PHONE_NUMBER_ALREADY_EXIST";
    public static final String ERROR_PERSISTING_DATA = "ERROR_PERSISTING_DATA";
    public static final String USER_DOES_NOT_EXIST = "USER_DOES_NOT_EXIST";
    public static final String INVALID_PHONENO_FORMAT = "INVALID_PHONENO_FORMAT";
    public static final String NO_OWNER_ID = "NO_OWNER_ID";
    public static final String BUSINESS_NAME_EMPTY = "BUSINESS_NAME_EMPTY";
    public static final String BUSINESS_NAME_ALREADY_EXIST = "BUSINESS_NAME_ALREADY_EXIST";
    public static final String BUSINESS_DOES_NOT_EXIST = "BUSINESS_DOES_NOT_EXIST";
    public static final String KIOSK_NAME_EMPTY = "KIOSK_NAME_EMPTY";
    public static final String NO_BUSINESS_ID = "NO_BUSINESS_ID";
    public static final String NO_KIOSK_ADDRESS = "NO_KIOSK_ADDRESS";
    
    public final static String PASSWORD_CHARGE_SUBJECT = "Chef Tent - Password Reset";

    public static final String KIOSK_DOES_NOT_EXIST = "KIOSK_DOES_NOT_EXIST";
    public static final String SUCCESS = "SUCCESS";
    public static final String PENDING = "PENDING";

    //Regex Validation
    public static final String PHONE_NUMBER_REGEX = "/^\\+(?:[0-9] ?){6,14}[0-9]$/";

    //Push Notofication Constants
    public static final String PUSH_NOTIFICATION_API_KEY = "AIzaSyCbgO1jDQ87Z3SR7DHBREzVaDQ7FmPOqAc";
    public static final String SYNC_USER = "sync_user";
    public static final String SYNC_DATA = "sync_data";
    public static final String SYNC_PERMISSION = "sync_permission";
    public static final String TEST = "test";
    public static final String SYNC_TIMELINE = "sync_timeline";
    public static final String SYNC_BACK_OFFICE = "sync_back_office";
    public static final String SYNC_ORDER_TIMELINE = "sync_order_timeline";
    
    //User Groups
    public static final String ATTENDANT = "Attendant";
    public static final String ACCOUNTANT = "Accountant";
    public static final String LOGISTICS_MANAGER = "Logistics Manager";
    public static final String DISPATCH_RIDER = "Dispatch Rider";
    
    public static final String PAYSTACK = "Paystack";
    public static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";
    
    public static final String PAYSTACK_INITIALIZE_PAYMENT_URL = "https://api.paystack.co/transaction/initialize";
    public static final String PAYSTACK_VERIFY_PAYMENT_URL = "https://api.paystack.co/transaction/verify/";
    public static final String PAYSTACK_RESPONSE_URL = "/service/paystackPostPaymentResponse";
    
    public static final String GEO_API ="AIzaSyBBPFRD9W0ohojPZzSk-1fUiN3UvXaQTyQ";
}

/**
*Class Name: PaystackInitializeResponse
*Project Name: Kiosk
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Aug 25, 2016 5:01:39 PM
*(C)Morph Innovations Limited 2016. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaystackInitializeResponse implements Serializable{

    private boolean status;
    
    private String message;
    
    private Data data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    
    
}

/**
 * Class Name: UserModel Project Name: Kiosk Developer: Onyedika Okafor
 * (ookafor@morphinnovations.com) Version Info: Create Date: Nov 17, 2016
 * 2:44:41 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel {

    private String firstName;
    private String surname;
    private String emailAddress;
    private String phoneNo;
    private String middleName;
    private Long id;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}

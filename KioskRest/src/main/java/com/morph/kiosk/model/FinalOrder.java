/**
 * Class Name: FinalOrder Project Name: Kiosk Developer: Onyedika Okafor
 * (ookafor@morphinnovations.com) Version Info: Create Date: Sep 15, 2016
 * 2:58:58 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.morph.kiosk.persistence.entity.order.DeliveryAddress;
import com.morph.kiosk.persistence.entity.order.KioskDeliveryOption;
import com.morph.kiosk.persistence.entity.order.KioskPaymentOption;
import java.util.Date;
import java.util.List;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FinalOrder {

    private Long id;

    private Date createdDate;

    private Date checkedOutDate;

    private Date updatedDate;
    
    private List<TrayItem> items;

    private UserModel orderedBy;
    
    private UserModel updatedby;

    private KioskDeliveryOption deliveryOption;

    private String coupon;
    
    private Double promotionDiscount;

    private Double totalAmount;
    
    private Double tax;
    
    private Double taxRate;
    
    private Double deliveryFee;

    private DeliveryAddress deliveryAddress;

    private KioskPaymentOption paymentOption;

    private String currency;

    private KioskViewMode kiosk;

    private String status;
    
    private String paymentStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public DeliveryAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public KioskViewMode getKiosk() {
        return kiosk;
    }

    public void setKiosk(KioskViewMode kiosk) {
        this.kiosk = kiosk;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCheckedOutDate() {
        return checkedOutDate;
    }

    public void setCheckedOutDate(Date checkedOutDate) {
        this.checkedOutDate = checkedOutDate;
    }

    public List<TrayItem> getItems() {
        return items;
    }

    public void setItems(List<TrayItem> items) {
        this.items = items;
    }

    public UserModel getOrderedBy() {
        return orderedBy;
    }

    public void setOrderedBy(UserModel orderedBy) {
        this.orderedBy = orderedBy;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public KioskDeliveryOption getDeliveryOption() {
        return deliveryOption;
    }

    public void setDeliveryOption(KioskDeliveryOption deliveryOption) {
        this.deliveryOption = deliveryOption;
    }

    public KioskPaymentOption getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(KioskPaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public UserModel getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(UserModel updatedby) {
        this.updatedby = updatedby;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(Double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public Double getPromotionDiscount() {
        return promotionDiscount;
    }

    public void setPromotionDiscount(Double promotionDiscount) {
        this.promotionDiscount = promotionDiscount;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }
    
   
    
    

}

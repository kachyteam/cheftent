/**
*Class Name: FoodVariation
*Project Name: Kiosk
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Nov 30, 2016 7:32:00 PM
*(C)Morph Innovations Limited 2016. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FoodVariation {

    private String description;
    
    private Double price;
    
    private String currency;
    
    private List<ItemViewMode> subItems;
    
    private List<SubItemGroupViewMode> subItemGroups;
    
    private Long id;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ItemViewMode> getSubItems() {
        return subItems;
    }

    public void setSubItems(List<ItemViewMode> subItems) {
        this.subItems = subItems;
    }

    public List<SubItemGroupViewMode> getSubItemGroups() {
        return subItemGroups;
    }

    public void setSubItemGroups(List<SubItemGroupViewMode> subItemGroups) {
        this.subItemGroups = subItemGroups;
    }
   
    
    
}

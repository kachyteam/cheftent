/**
*Class Name: SubItemGroup
*Project Name: Kiosk
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Dec 1, 2016 11:34:02 AM
*(C)Morph Innovations Limited 2016. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubItemGroupViewMode {

    private String name;
    
    private List<ItemViewMode> subItems;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ItemViewMode> getSubItems() {
        return subItems;
    }

    public void setSubItems(List<ItemViewMode> subItems) {
        this.subItems = subItems;
    }
    
    
}

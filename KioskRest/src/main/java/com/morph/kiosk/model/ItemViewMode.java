/**
*Class Name: ItemViewMode
*Project Name: KioskEJBModuleV2
*Developer: Onyedika Okafor (ookafor@morphinnovations.com)
*Version Info:
*Create Date: Aug 18, 2016 11:16:26 AM
*(C)Morph Innovations Limited 2016. Morph Innovations Limited Asserts its right to be known
*as the author and owner of this file and its contents.
*/

package com.morph.kiosk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemViewMode implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Long id;
    
    private String name;
    private Boolean available;
    private List<ItemViewMode> subItems;
    private Date dateAdded;
    private Integer max;
    private Double price;
    private String imageUrl;
    private String currency;
    private String description;
    private List<FoodVariation> variation;
    private List<SubItemGroupViewMode> subItemGroups;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public List<ItemViewMode> getSubItems() {
        return subItems;
    }

    public void setSubItems(List<ItemViewMode> subItems) {
        this.subItems = subItems;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FoodVariation> getVariation() {
        return variation;
    }

    public void setVariation(List<FoodVariation> variation) {
        this.variation = variation;
    }

    public List<SubItemGroupViewMode> getSubItemGroups() {
        return subItemGroups;
    }

    public void setSubItemGroups(List<SubItemGroupViewMode> subItemGroups) {
        this.subItemGroups = subItemGroups;
    }

    
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemViewMode)) {
            return false;
        }
        ItemViewMode other = (ItemViewMode) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.morph.kiosk.persistence.entities.item.ItemViewMode[ id=" + id + " ]";
    }

}

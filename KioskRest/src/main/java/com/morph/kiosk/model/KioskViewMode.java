/**
 * Class Name: KioskViewMode Project Name: Kiosk Developer: Onyedika Okafor
 * (ookafor@morphinnovations.com) Version Info: Create Date: Aug 22, 2016
 * 2:04:56 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class KioskViewMode implements Serializable {

    private Long id;

    private String name;
    private String description;

    private String logo;
    
    private boolean closed;
    
    private Integer minDeliveryTime;
    private boolean allowSchedule;

    public KioskViewMode() {
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public Integer getMinDeliveryTime() {
        return minDeliveryTime;
    }

    public void setMinDeliveryTime(Integer minDeliveryTime) {
        this.minDeliveryTime = minDeliveryTime;
    }

    public boolean isAllowSchedule() {
        return allowSchedule;
    }

    public void setAllowSchedule(boolean allowSchedule) {
        this.allowSchedule = allowSchedule;
    }

    
    
    
    

}

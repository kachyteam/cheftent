/**
 * Class Name: OrderTray Project Name: Kiosk Developer: Onyedika Okafor
 * (ookafor@morphinnovations.com) Version Info: Create Date: Aug 23, 2016
 * 2:16:02 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.morph.kiosk.persistence.entity.Kiosk;
import com.morph.kiosk.persistence.entity.order.DeliveryAddress;
import java.io.Serializable;
import java.util.List;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderTray implements Serializable {

    private Long id;

    private List<TrayItem> items;

    private String coupon;

    private Integer paymentOption;

    private Integer deliveryOption;

    private DeliveryAddress address;

    private Long kiosk;
    
    private Long deliveryTime;
    
    private Kiosk place;

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public List<TrayItem> getItems() {
        return items;
    }

    public void setItems(List<TrayItem> items) {
        this.items = items;
    }

    public Integer getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(Integer paymentOption) {
        this.paymentOption = paymentOption;
    }

    public Integer getDeliveryOption() {
        return deliveryOption;
    }

    public void setDeliveryOption(Integer deliveryOption) {
        this.deliveryOption = deliveryOption;
    }

    public DeliveryAddress getAddress() {
        return address;
    }

    public void setAddress(DeliveryAddress address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getKiosk() {
        return kiosk;
    }

    public void setKiosk(Long kiosk) {
        this.kiosk = kiosk;
    }

    public Long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Kiosk getPlace() {
        return place;
    }

    public void setPlace(Kiosk place) {
        this.place = place;
    }

       
}

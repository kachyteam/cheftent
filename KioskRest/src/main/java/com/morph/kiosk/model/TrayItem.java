/**
 * Class Name: TrayItem Project Name: Kiosk Developer: Onyedika Okafor
 * (ookafor@morphinnovations.com) Version Info: Create Date: Aug 23, 2016
 * 2:34:54 PM (C)Morph Innovations Limited 2016. Morph Innovations Limited
 * Asserts its right to be known as the author and owner of this file and its
 * contents.
 */
package com.morph.kiosk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

/**
 * @author Onyedika Okafor (ookafor@morphinnovations.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrayItem {

    private Food food;
    private int quantity;
    private String instruction;
    private FoodVariation variation;
    private List<Food> subItems;

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public FoodVariation getVariation() {
        return variation;
    }

    public void setVariation(FoodVariation variation) {
        this.variation = variation;
    }

    public List<Food> getSubItems() {
        return subItems;
    }

    public void setSubItems(List<Food> subItems) {
        this.subItems = subItems;
    }

}
